const express = require("express")
const app = express()
const connectDB = require("./util/db").connectDb
const cors = require("cors")
const {Router} = require("./Routes/Main")


app.use(express.json({ limit: "20mb" }))
app.use(express.urlencoded({ extended: false }))
app.use(cors())

Router(app)


connectDB(()=>{
    app.listen(3000)
})