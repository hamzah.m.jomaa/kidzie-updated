const Nationality = require("../Models/Nationality");
const Product = require("../Models/product");

exports.Search = async (req, res) => {
  const { search } = req.body;
  const qGroup = search.group ? { group: search.group } : {};
  const qGender = search.gender ? { gender: search.gender } : {};
  const qonSale = search.onSale == false || search.onSale == true ? { onSale: search.onSale } : {};

  const returnedProducts = await Product.aggregate([
    { $match: { $and: [qGroup, qGender, qonSale] } },
  ]);
  return res.status(200).json(returnedProducts);
};

exports.SearchGet = async (req, res) => {
  const products = await Product.find({});
  return res.status(200).json({
    status: 200,
    data: {
      products,
    },
  });
};

exports.addProduct = async (req, res) => {
  // const {product} = req.body
  // product.forEach(async element => {
  //     element.date = new Date(element.date)
  //     const createdProduct = await Product.create(element)
  // });
  // res.status(200).json({status:"added"})
};

exports.getData = async (req, res) => {
  const nationality = await Nationality.find({});
  return res.status(200).json({
    status: 200,
    data: {
      nationality,
    },
  });
};
