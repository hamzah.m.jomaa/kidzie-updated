const bcrypt = require("bcrypt");
const User = require("../Models/User");


exports.updateProfile = async (req,res)=>{
  let {user,password,UserId} = req.body
  user.password = password? await bcrypt.hash(password,12): user.password
  const updatedUser = await User.findByIdAndUpdate(UserId,user)
  return res.json({
    status:200,
    user: updatedUser
  })
}

exports.Profile = async (req,res) =>{
  const {userId} = req.body
  const getUser = await User.findById(userId)


  if (getUser){
    return res.json({
      status: 200,
      message: "User Found",
      user: getUser
    });
  }

  return res.json({
    status: 400,
    message: "No User Found",
    data: ""
  });

}


exports.Login = async (req, res) => {
  const { username, password } = req.body;

  const getUser = await User.findOne({ username });
  if (!getUser) {
    return res.json({
      status: 400,
      message: "No User Found",
      data: ""
    });
  }

  const checkedPassword = await bcrypt.compare(password, getUser.password);
  console.log()
  if (!checkedPassword) {
    return res.json({
      status: 400,
      message: "Wrong Password",
      data: ""
    });
  }
  return res.json({
    status: 200,
    message: "Success",
    data:{
      userId: getUser._id,
      username: getUser.username
    }
  });
};

exports.Register = async (req, res) => {
  const {
    firstName,
    lastName,
    email,
    phoneNumber,
    country,
    gender,
    username,
    password,
  } = req.body;
  
  const user = await User.findOne({ username });

  if (user) {
    return res.status(409).json({
        status: 409,
        messasge: "User Already Exists"
    });
  }

  const hashedPassword = await bcrypt.hash(password, 12);

  const created_User = await User.create({
    firstName,
    lastName,
    email,
    phoneNumber,
    country,
    gender,
    username,
    password: hashedPassword,
  });

  return res.status(200).json({ 
      status: 200,
      message: "User Created",
      data: created_User
   });
};
