const express = require("express");
const router = express.Router();
const AdminController = require("../Controllers/AdminControllers")

router.post("/search",AdminController.Search)
router.get("/search",AdminController.SearchGet)
router.post("/add",AdminController.addProduct)

module.exports = router