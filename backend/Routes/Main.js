const Auth = require("./Auth")
const getRequests = require("./getRequests")
const Profile = require("./profile")
const Products = require("./products")

module.exports.Router = (app) =>{
    app.use("/api/",getRequests)
    app.use("/api/products",Products)
    app.use("/api/profile",Profile)
    app.use("/api/user",Auth)
}